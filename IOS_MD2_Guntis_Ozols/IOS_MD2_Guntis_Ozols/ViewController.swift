//
//  ViewController.swift
//  IOS_MD2_Guntis_Ozols
//
//  Created by Gman on 23/05/2018.
//  Copyright © 2018 Guntis Ozols. All rights reserved.
//
import WebKit
import UIKit


class ViewController: UIViewController, WKUIDelegate, WKNavigationDelegate {
    
    var rez, tokens: String?
    
    @IBOutlet weak var thisview: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let site =  "https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id=4994b1b4-2a7f-44a7-8405-ebe132ba2ba8&scope=files.read&response_type=code&redirect_uri=msal4994b1b4-2a7f-44a7-8405-ebe132ba2ba8://auth"
        
        let url = URL(string:site.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        
        let req = URLRequest(url: url!)
        thisview.navigationDelegate = self as WKNavigationDelegate;
        thisview.load(req)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        if (rez != nil) {
            let url = URL(string: "https://login.microsoftonline.com/common/oauth2/v2.0/token")!
            var request = URLRequest(url: url)
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            let client = "4994b1b4-2a7f-44a7-8405-ebe132ba2ba8"
            
            
            let postString = "client_id="+client+"&redirect_uri=msal4994b1b4-2a7f-44a7-8405-ebe132ba2ba8://auth&code=\(rez!)&grant_type=authorization_code"
            request.httpBody = postString.data(using: .utf8)
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {
                    print("error=\(error)")
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                }
                
                let responseString = String(data: data, encoding: .utf8)
                self.webView2(responseString: responseString!)
                let dict = self.convertToDictionary(text: responseString!)
                let tokens =  dict!["access_token"] as! String
                self.getDrive(token: tokens)
            }
            task.resume()
        }
    }
    
    func getDrive(token: String){
        
        let url = URL(string: "https://graph.microsoft.com/v1.0/me/drives/07CE734DFA9959DD/root/children")!
        var request = URLRequest(url: url)
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print(responseString)
            
            DispatchQueue.main.async { [unowned self] in
                self.thisview.loadHTMLString(responseString!, baseURL: nil)
            }
            
        }
        task.resume()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let teksts = navigationAction.request.url?.absoluteString
        if teksts?.range(of:"code=") != nil {
            let tekstsCom = teksts?.components(separatedBy: "=")
            rez = tekstsCom![1]
            print(rez!)
        }
        
        decisionHandler(WKNavigationActionPolicy.allow)
    }
    
    func webView2(responseString: String) {
        
        let tekstsCom = responseString.components(separatedBy: ",")
        print(tekstsCom.last!)
        
    }
}

